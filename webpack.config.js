const path = require('path');
const webpack = require('webpack');
const HtmlWebpackPlugin = require('html-webpack-plugin');

const config = {
    plugins: [
        // do not bundle all moment.js locales (saves >200kB in size per entry point)
        new webpack.IgnorePlugin(/^\.\/locale$/, /moment$/),

        new HtmlWebpackPlugin({
            hash: true,
            template: './src/index.html.ejs',
            filename: './index.html', //relative to root of the application
            //favicon: './src/assets/favicon.ico',
            title: 'webpack-template'
        })
    ],
    entry: {
        app: './src/main.ts'
    },
    output: {
        path: path.resolve(__dirname, 'dist')
    },
    resolve: {
        // Add `.ts` and `.tsx` as a resolvable extension.
        extensions: [".ts", ".tsx", ".js"]
    },
    module: {
        rules: [
            {
                parser: { system: false }
            },
            // all files with a `.ts` or `.tsx` extension will be handled by `ts-loader`
            {
                test: /\.tsx?$/,
                use: [
                    {
                        loader: 'babel-loader'
                    },
                    {
                        loader: 'ts-loader',
                        options: {
                            configFile: 'tsconfig.json'
                        }
                    }
                ]
            },
            // load JavaScript files
            {
                test: /\.js$/,
                exclude: /(lib)/,
                use: {
                    loader: 'babel-loader',
                    options: {
                        presets: ['@babel/env']
                    }
                }
            },
            // load YAML files as JSON
            {
                test: /\.yml$/,
                use: [ 'json-loader', 'yaml-loader' ]
            },
            {
                test: /\.html$/,
                use: [ 'html-loader' ]
            },
            {
                test:/\.css$/,
                use:['style-loader','css-loader']
            },
            {
                test: /\.(png|jpg|gif|ico)$/,
                use: [
                    {
                        loader: 'file-loader',
                        options: {}
                    }
                ]
            }
        ]
    },
    devServer: {
        contentBase: path.join(__dirname, 'dist'),
        compress: true,
        port: 9000
    }
};

module.exports = config;
