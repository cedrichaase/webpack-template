# Webpack Template

A simple starting point for a TypeScript application, bundled with webpack

## Usage


### Development

```sh
npm install
npm run dev
```

### Production build

```sh
webpack --mode="production"
```

